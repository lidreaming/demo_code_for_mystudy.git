/** @file         client.c
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2021-06-10 09:49:46
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>

#define sHOST "192.168.112.128" // 服务器端IP
#define sPORT 8080             // 服务器进程端口号

#define BUFFER_SIZE (1*1024)   // 数据域大小

// 注意: 接收缓冲区对应对方的发送缓冲区小于等于就不会出现对方接收多次。


/**
  * @brief  读 线程
  * @param 
  * @retval 
  * @author
  */
void *client_read_fun(void *arg) 
{
	int recv_len = 0; // 接收长度
    char recv_buff[BUFFER_SIZE] = "";
    int sockfd = *(int *)arg; // 已连接的 socket

    while((recv_len = recv(sockfd, recv_buff, sizeof(recv_buff), 0)) > 0)
    {
        printf("[%d]recv_buff:%s\r\n", sockfd, recv_buff);
    }

    printf("exit read thread!");
    return NULL;
}

/**
  * @brief  封装写函数
  * @param 
  * @retval 
  * @author
  */
ssize_t writen(int fd, const void *vptr, size_t n)
{
    size_t nleft; // 剩余要写的字节数
    ssize_t nwritten; // 已经写的字节数
    const char *ptr; // write的缓冲区

    ptr = vptr; // 备份
    nleft = n; // 初始化剩余字节数

    // 一直写，直至写完或出错
    while( nleft>0 )
    {
        if((nwritten = write(fd, ptr, nleft)) <= 0)
        {
            if(nwritten < 0 && errno == EINTR) // 信号被打断
                nwritten = 0; // 本次已写0个
            else
                return(-1); // 其它错误
        }
        nleft -= nwritten; // 剩余需要写的
        ptr += nwritten; // 下次开始写的
    }
    return(n); //返回已经写了的字节数
}

/**
  * @brief  主函数
  * @param 
  * @retval 
  * @author
  */
int main(void)
{
    int sockfd; // 套接字描述符
    int ret;    // 返回结果
    struct sockaddr_in server; // 套接字地址
    char data_buffer[BUFFER_SIZE];  // app 数据域
    pthread_t thread_read_id; // 读 线程 id

    /* [1] 创建套接字 */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1)
    {
        printf("creat socket failed!\n");
        exit(1);
    }

    /* [2] 初始化地址 */
    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(sPORT);
    server.sin_addr.s_addr = inet_addr(sHOST);

    /* [3] 建立TCP连接 */
    ret = connect(sockfd, (struct sockaddr *)&server, sizeof(struct sockaddr));
    if(ret == -1)
    {
        printf("connect server failed!\n");
        close(sockfd);
        exit(1);
    }

    printf("connect server success!");

    /* [4][1] 创建 读 线程 */
    //pthread_create(&thread_read_id, NULL, (void *)client_read_fun, (void *)&(sockfd));
    //pthread_detach(thread_read_id); // 线程分离

    /* [4][2] 进入循环 写 */
    while(1)
    {
        printf("please enter some text:");
        fgets(data_buffer, BUFFER_SIZE, stdin);

        // 输入end，退出循环
        if(strncmp(data_buffer, "end", 3) == 0)
            break;


        //write(sockfd, data_buffer, sizeof(data_buffer));
        send(sockfd, data_buffer, sizeof(data_buffer), 0);

        bzero(data_buffer, sizeof(data_buffer));

        recv(sockfd, data_buffer, sizeof(data_buffer), 0);

        printf("data_buff:%s\r\n", data_buffer);

    }

    /* [5] 退出 */
    close(sockfd);
    exit(0);
}
