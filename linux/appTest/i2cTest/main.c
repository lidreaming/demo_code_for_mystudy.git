/** @file         main.c
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2020-11-28 19:18:20
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/ioctl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<sys/select.h>
#include<sys/time.h>

#include "mpu6050/bsp_mpu6050.h"

int main(int argc, char * argv[])
{
	/* 程序开始标语 */
	printf("This is a mpu6050 test!\n");

	/* 初始化 MCPU6050 */
	mpu6050_init("/dev/i2c-0");
	sleep(1);

	/* mpu6050 应用测试 */
	while(1)
	{
		printf("get mpu6050 data!\n");
        usleep(1000 * 100);
        printf("ACCE_X:%6d\n ", getData(ACCEL_XOUT_H));
        usleep(1000 * 100);
        printf("ACCE_Y:%6d\n ", getData(ACCEL_YOUT_H));
        usleep(1000 * 100);
        printf("ACCE_Z:%6d\n ", getData(ACCEL_ZOUT_H));
        usleep(1000 * 100);
        printf("GYRO_X:%6d\n ", getData(GYRO_XOUT_H));
        usleep(1000 * 100);
        printf("GYRO_Y:%6d\n ", getData(GYRO_YOUT_H));
        usleep(1000 * 100);
        printf("GYRO_Z:%6d\n\n ", getData(GYRO_ZOUT_H));
        sleep(1);
	}

	/* 退出 mpu6050 */
	mpu6050_close();
}
