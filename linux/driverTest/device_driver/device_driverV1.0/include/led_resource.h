/** @file         led_resource.h
 *  @brief        资源格式文件。
 *  @details      提供led资源结构体，供给device + driver。
 *  @author       lzm
 *  @date         2021-03-06 10:20:03
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#include <linux/io.h>

/* led 资源编程模式 */
#define LED_RESOURCE_MODE 1


#if (LED_RESOURCE_MODE == 0) // 还没有找到快速库
/* pin 快速库 */
#define GET_GROUP(x)    (x>>16)
#define GET_PIN(x)      (x&0xFFFF)
#define GROUP_PIN(g, p) ((g<<16) | (p))

/* led 资源结构体 */
typedef struct 
{
    int pin; // 引脚数据 high 16 bit for gpio group and low 16 bit for gpio pin.
}led_resource_t;

#elif (LED_RESOURCE_MODE == 1) //粗暴版

/* led 资源结构体 */
typedef struct 
{
    unsigned long pa_dr; // 数据寄存器  物理地址
	unsigned long pa_gdir; // 输入输出寄存器  物理地址
	unsigned long pa_iomuxc_mux; // 端口复用寄存器  物理地址
	unsigned long pa_ccm_ccgrx; // 端口时钟寄存器  物理地址
	unsigned long pa_iomux_pad; // 电气属性寄存器  物理地址

    unsigned int pin; // 引脚号
    unsigned int clock_offset; // 时钟偏移
}led_resource_t;

#endif // [LED_RESOURCE_MODE]

/* led 资源结构体访问函数（具体实现有设备文件提供） */
led_resource_t *get_led_resource(void);

/* 广播 */
extern led_resource_t;