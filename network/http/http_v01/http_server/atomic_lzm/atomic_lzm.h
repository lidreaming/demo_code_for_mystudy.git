/** @file         atomic_lzm.h
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2021-06-10 16:49:46
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#ifndef  __ATOMIC_LZM_H_
#define  __ATOMIC_LZM_H_


#include <pthread.h>

typedef struct
{
    pthread_mutex_t mutex;
    int num;
}atomic_lzm_t;


void atomic_lzm_init(atomic_lzm_t *atomic_x, int num);

void atomic_lzm_add(atomic_lzm_t *atomic_x);

void atomic_lzm_sub(atomic_lzm_t *atomic_x);

void atomic_lzm_set(atomic_lzm_t *atomic_x, int num);

int atomic_lzm_get(atomic_lzm_t *atomic_x);

#endif // ! __ATOMIC_LZM_H__