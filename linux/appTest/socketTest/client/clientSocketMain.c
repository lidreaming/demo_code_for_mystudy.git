#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define sHOST "192.168.43.63" // 服务器端IP
#define sPORT 1234             // 服务器进程端口号

#define BUFFER_SIZE (4*1024)   // 数据域大小

/**
  * @brief  封装写函数
  * @param 
  * @retval 
  * @author
  */
ssize_t writen(int fd, const void *vptr, size_t n)
{
    size_t nleft; // 剩余要写的字节数
    ssize_t nwritten; // 已经写的字节数
    const char *ptr; // write的缓冲区

    ptr = vptr; // 备份
    nleft = n; // 初始化剩余字节数

    // 一直写，直至写完或出错
    while( nleft>0 )
    {
        if((nwritten = write(fd, ptr, nleft)) <= 0)
        {
            if(nwritten < 0 && errno == EINTR) // 信号被打断
                nwritten = 0; // 本次已写0个
            else
                return(-1); // 其它错误
        }
        nleft -= nwritten; // 剩余需要写的
        ptr += nwritten; // 下次开始写的
    }
    return(n); //返回已经写了的字节数
}

int main(void)
{
    int sockfd; // 套接字描述符
    int ret;    // 返回结果
    struct sockaddr_in server; // 套接字地址
    char data_buffer[BUFFER_SIZE];  // app 数据域

    /* 1. 创建套接字 */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1)
    {
        printf("creat socket failed!\n");
        exit(1);
    }

    bzero(&server, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(sPORT);
    server.sin_addr.s_addr = inet_addr(sHOST);

    /* 2. 建立TCP连接 */
    ret = connect(sockfd, (struct sockaddr *)&server, sizeof(struct sockaddr));
    if(ret == -1)
    {
        printf("connect server failed!\n");
        close(sockfd);
        exit(1);
    }

    printf("connect server success!");

    while(1)
    {
        printf("please enter some text:");
        fgets(data_buffer, BUFFER_SIZE, stdin);

        // 输入end，退出循环
        if(strncmp(data_buffer, "end", 3) == 0)
            break;

        write(sockfd, data_buffer, sizeof(data_buffer));
    }

    close(sockfd);
    exit(0);
}
