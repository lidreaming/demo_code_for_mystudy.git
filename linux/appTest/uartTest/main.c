/** @file         main.c
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2020-11-23 19:18:20
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <string.h>
#include <sys/ioctl.h>

/* 不同的设备，不同的路径 */
const char def_uart_path[] = "/dev/ttymxc2";


/**
 * @brief  主函数
 * @param  无
 * @retval 无
 */
int main(int argc, char *argv[])
{
	int   fd;
	int   res;
	char *path;
	char  bufW[512] = "This is sys uart test!\n";
	char  bufR[512];

	/* 终端设备选择 */
	if(argc > 1)
		path = argv[1];
	else
		path = (char *)def_uart_path;

	/* 打开终端 */
	fd = open(path, O_RDWR);
	if(fd < 0){
		printf("[%s] open err!", path);
		return 0;
	}

	/* 定义串口结构体 */
	struct termios opt;

	/* 清空串口接收缓冲区 */
	tcflush(fd, TCIOFLUSH);
	/* 获取串口参数 */
	tcgetattr(fd, &opt);

	/* 设置输入、输入波特率 */
	cfsetospeed(&opt, B9600);
	cfsetispeed(&opt, B9600);
	/* 设置数据位数 */
	opt.c_cflag &= ~CSIZE;
	opt.c_cflag |= CS8;
	/* 校验位 */
	opt.c_cflag &= ~PARENB;
	opt.c_iflag &= ~INPCK;
	/* 设置停止位 */
	opt.c_cflag &= ~CSTOPB;

	/* 更新配置 */
	tcsetattr(fd, TCSANOW, &opt);

	do{
		/* 发送测试 */
		write(fd, bufW, strlen(bufW));

		/* 接收测试 */
		res = read(fd, bufR, 512);
		if(res > 0){
			printf("receive data is %s", bufR);
		}
	}while(res >= 0);

	/* 读取错误 */
	printf("read error, res = %d", res);

	/* 关闭文件 */
	close(fd);
	return 0;
}

