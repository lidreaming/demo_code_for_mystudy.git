/** @file         atomic_lzm.c
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2021-06-10 16:49:46
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/


#include "atomic_lzm.h"

void atomic_lzm_init(atomic_lzm_t *atomic_x, int num)
{
	pthread_mutex_init(&atomic_x->mutex, NULL);	
    atomic_x->num = num;
}

void atomic_lzm_add(atomic_lzm_t *atomic_x)
{
	pthread_mutex_lock(&atomic_x->mutex);	
    atomic_x->num++;
	pthread_mutex_unlock(&atomic_x->mutex);	
}


void atomic_lzm_sub(atomic_lzm_t *atomic_x)
{
	pthread_mutex_lock(&atomic_x->mutex);	
    atomic_x->num--;
	pthread_mutex_unlock(&atomic_x->mutex);	
}

void atomic_lzm_set(atomic_lzm_t *atomic_x, int num)
{
	pthread_mutex_lock(&atomic_x->mutex);	
    atomic_x->num = num;
	pthread_mutex_unlock(&atomic_x->mutex);	
}


int atomic_lzm_get(atomic_lzm_t *atomic_x)
{
    return atomic_x->num;
}
