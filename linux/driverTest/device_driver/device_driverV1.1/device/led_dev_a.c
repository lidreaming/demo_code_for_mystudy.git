/** @file         led_dev_a.c
 *  @brief        具体设备。涉及硬件，具体板卡A
 *  @details      提供具体led引脚
 *  @author       lzm
 *  @date         2021-03-06 10:17:03
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#include "led_dev_a.h"

/* 红灯 */
led_resource_t led_r=
{
    .pa_dr = 0x0209C000,
    .pa_gdir = 0x0209C004,
    .pa_iomuxc_mux = 0x20E006C,
    .pa_ccm_ccgrx = 0x20C406C,
    .pa_iomux_pad = 0x20E02F8,
    .pin = 4,
    .clock_offset = 26,
};

/* 绿灯 */
led_resource_t led_g=
{
    .pa_dr = 0x020A8000,
    .pa_gdir = 0x020A8004,
    .pa_iomuxc_mux = 0x020E01E0,
    .pa_ccm_ccgrx = 0x020C4074,
    .pa_iomux_pad = 0x020E046C,
    .pin = 20,
    .clock_offset = 12,
};

/* 蓝灯 */
led_resource_t led_b=
{
    .pa_dr = 0x020A800,
    .pa_gdir = 0x020A8004,
    .pa_iomuxc_mux = 0x020E01DC,
    .pa_ccm_ccgrx = 0x020C4074,
    .pa_iomux_pad = 0x020E0468,
    .pin = 19,
    .clock_offset = 12,
};

led_resource_t *get_led_resource(char ch)
{
    if(ch == 'R' || ch == 'r' || ch == '0')
        return &led_r;
    else if(ch == 'G' || ch == 'g' || ch == '1')
        return &led_g;
    else if(ch == 'B' || ch == 'b' || ch == '2')
        return &led_b;
    
    return 0;
}
