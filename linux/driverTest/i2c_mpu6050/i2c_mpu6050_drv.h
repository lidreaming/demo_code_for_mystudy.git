/** @file         i2c_mpu605m_drv.h
 *  @brief        led 平台驱动文件
 *  @details      基于驱动模块的方式实现 驱动注册/注销等等操作。
 *  @author       lzm
 *  @date         2021-06-30 20:22:03
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *  @blog         https://www.cnblogs.com/lizhuming/
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#ifndef __i2c_mpu6050_driver_h__
#define __i2c_mpu6050_driver_h__

#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/i2c.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/ide.h>
#include <linux/errno.h>
#include <linux/gpio.h>
#include <asm/mach/map.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_gpio.h>
#include <asm/io.h>
#include <linux/device.h>

#include <linux/platform_device.h>


#define MPU_SMPLRT_DIV                                  0x19
#define MPU_CONFIG                                      0x1A
#define MPU_GYRO_CONFIG                                 0x1B
#define MPU_ACCEL_CONFIG                                0x1C
#define MPU_ACCEL_XOUT_H                                0x3B
#define MPU_ACCEL_XOUT_L                                0x3C
#define MPU_ACCEL_YOUT_H                                0x3D
#define MPU_ACCEL_YOUT_L                                0x3E
#define MPU_ACCEL_ZOUT_H                                0x3F
#define MPU_ACCEL_ZOUT_L                                0x40
#define MPU_TEMP_OUT_H                                  0x41
#define MPU_TEMP_OUT_L                                  0x42
#define MPU_GYRO_XOUT_H                                 0x43
#define MPU_GYRO_XOUT_L                                 0x44
#define MPU_GYRO_YOUT_H                                 0x45
#define MPU_GYRO_YOUT_L                                 0x46
#define MPU_GYRO_ZOUT_H                                 0x47
#define MPU_GYRO_ZOUT_L                                 0x48
#define MPU_PWR_MGMT_1                                  0x6B
#define MPU_WHO_AM_I                                    0x75
#define MPU_SlaveAddress                                0xD0
#define MPU_Address                                     0x68                  //MPU6050地址
#define MPU_I2C_RETRIES                                 0x0701
#define MPU_I2C_TIMEOUT                                 0x0702
#define MPU_I2C_SLAVE                                   0x0703       //IIC从器件的地址设置
#define MPU_I2C_BUS_MODE                                0x0780

/* 常量 */
#define MPU6050_DEV_NAME "mpu6050_dev" // 设备名称
#define MPU6050_DEV_INODE_NAME "mpu6050_dev" // 设备节点名称前缀
#define MPU6050_DEV_CLASS "mpu6050_dev_class" // 设备类名称

/* led 数据结构体 */
struct MPU6050_DEV_T
{
    dev_t dev_num; // 设备号 (当然，cdev 里面也保存了设备号)
    struct cdev chr_dev; // 设备内核文件

    struct i2c_client *mpu6050_client;
};
typedef struct MPU6050_DEV_T mpu6050_dev_t;

#define MAX(a,b) ((a)>(b)?(a):(b))
#define MIN(a,b) ((a)<(b)?(a):(b))


#endif /* __i2c_mpu6050_driver_h__ */



