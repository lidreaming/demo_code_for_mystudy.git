/** @file         chrdev_app.c
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2021-02-26 15:33:15
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

char *wbuf = "Hello World\n";
char rbuf[128];

/** @brief  main
  * @param 
  * @retval 
  * @author lzm
  */
int main(void)
{
    printf("chrdev app test\n");

    //打开文件
    int fd = open("/dev/chrdev1", O_RDWR);
    //写入数据
    write(fd, wbuf, strlen(wbuf));
    //读取文件内容
    read(fd, rbuf, 128);
    //打印读取的内容
    printf("The content : %s", rbuf);
    //读取完毕，关闭文件
    close(fd);
    return 0;
}
