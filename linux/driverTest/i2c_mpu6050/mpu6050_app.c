/** @file         mpu6050_app.c
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2021-07-04 23:31:56
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *  @blog         https://www.cnblogs.com/lizhuming/
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

/** @brief main
  * @details 
  * @param 
  * @retval 
  * @author lzm
  */
int main(int argc, char *argv[])
{
    short receive_data[6] = {0};

    /* open mpu6050 file */
    int fd = open("/dev/mpu6050_dev_1", O_RDWR);
    if(fd < 0)
    {
        printf("%d-%s:open file mpu6050_1 faild!\n", __LINE__, __func__);
        return -1;
    }

    /* read data */
    int ret = read(fd, receive_data, 12);
    if(ret < 0)
    {
        printf("%d-%s:read file faild!\n", __LINE__, __func__);
        close(fd);
    }

    printf("AX=%d, AY=%d, AZ=%d \n", (int)receive_data[0], (int)receive_data[1], (int)receive_data[2]);
    printf("GX=%d, GY=%d, GZ=%d \n", (int)receive_data[3], (int)receive_data[4], (int)receive_data[5]);

    /* close file */
    ret = close(fd);
    if(ret < 0)
    {
        printf("%d-%s:close file faild!\n", __LINE__, __func__);
    }
    return 0;
}

