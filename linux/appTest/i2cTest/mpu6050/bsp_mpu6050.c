/** @file         bsp_mpu6050.c
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2020-11-28 19:20:20
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

/* 头文件 */
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/ioctl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<sys/select.h>
#include<sys/time.h>

#include "bsp_mpu6050.h"


/* 设备句柄 */
int i2cFd; // i2c设备句柄

/**
  * @brief  i2c 写
  * @param  fd：i2c设备句柄
  * @param  regAddr：寄存器地址
  * @param  val：需要写入的值
  * @retval 0：写入成功
  * @retval -1：写入失败
  * @author lzm
  */
static uint8_t i2c_write(int fd, uint8_t regAddr, uint8_t val)
{
    int cnt; // 写入失败后，重复写入的次数
    uint8_t data[2]; // data[0]为寄存器地址，data[1]为需要写入的值

    data[0] = regAddr;
    data[1] = val;

    for(cnt=5; cnt>0; cnt--)
    {
        if(write(fd, data, 2) == 2)
            return 0; // 写入成功
    }
    return -1; // 写入失败
}

/**
  * @brief  i2c 读
  * @param  fd：i2c设备句柄
  * @param  regAddr：寄存器地址
  * @param  val：读取到数据保存的地方
  * @retval 0：读取成功
  * @retval -1：读取失败
  * @author lzm
  */
static uint8_t i2c_read(int fd, uint8_t regAddr, uint8_t * val)
{
    int cnt; // 读取失败后，重新读取的次数

    for(cnt=5; cnt>0; cnt--)
    {
        if(write(fd, &regAddr, 1) == 1)
        {
            if(read(fd, val, 1) == 1)
                return 0;
        }
    }
    return -1;
}


/**
  * @brief  mpu6050初始化
  * @param  i2cDev
  * @retval 1:初始化成功
  * @retval 0:初始化失败
  * @author lzm
  */
uint8_t mpu6050_init(char * i2cDev)
{
    i2cFd = open(i2cDev, O_RDWR); // 打开i2c设备文件
    if(i2cFd < 0)
    {
        printf("Can't open %s!\n", i2cDev);
        exit(1);
    }
    printf("Open %s success!", i2cDev);

    if(ioctl(i2cFd, I2C_SLAVE, Address) < 0)
    {
        printf("fail to set i2c device slave address!");
        close(i2cFd); // 关闭i2c设备文件
        return -1;
    }

    printf("set slave address to 0x%x success!", Address);
    i2c_write(i2cFd, PWR_MGMT_1, 0X00);
    i2c_write(i2cFd, SMPLRT_DIV, 0X07);
    i2c_write(i2cFd, CONFIG, 0X06);
    i2c_write(i2cFd, ACCEL_CONFIG, 0X01);
    return 1;
}

/**
  * @brief  关闭设备文件
  * @param  
  * @retval 
  * @author lzm
  */
void mpu6050_close(void)
{
    close(i2cFd);
}

/**
  * @brief  获取数据
  * @param  regAddr:寄存器地址
  * @retval 获取到的数据
  * @author lzm
  */
short getData(unsigned char regAddr)
{
    char chH; // 高字节
    char chL; // 低字节

    i2c_read(i2cFd, regAddr, &chH);
    usleep(1000);
    i2c_read(i2cFd, regAddr, &chL);
    return ((chH << 8) + chL);
}

