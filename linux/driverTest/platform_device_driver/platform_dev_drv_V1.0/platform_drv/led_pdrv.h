/** @file         led_drv.h
 *  @brief        led 平台驱动文件
 *  @details      基于驱动模块的方式实现 驱动注册/注销等等操作。
 *  @author       lzm
 *  @date         2021-03-30 20:22:03
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#ifndef __led_drv_h__
#define __led_drv_h__

#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mod_devicetable.h>
#include <linux/io.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

/* 常量 */
#define LED_DEV_NAME "led_cdev" // 设备名称
#define LED_DEV_INODE_NAME "led_cdev" // 设备节点名称前缀
#define LED_DEV_CLASS "led_cdev_class" // 设备类名称

/* led 数据结构体 */
struct LED_DATA_T
{
	unsigned int __iomem *va_ccm_ccgrx; // 端口时钟寄存器  虚拟地址
	unsigned int __iomem *va_iomuxc_mux; // 端口复用寄存器  虚拟地址
	unsigned int __iomem *va_iomux_pad; // 电气属性寄存器  虚拟地址
	unsigned int __iomem *va_gdir; // 输入输出寄存器  虚拟地址
    unsigned int __iomem *va_dr; // 数据寄存器  虚拟地址

    unsigned int pin; // 引脚号
    unsigned int clock_offset; // 时钟偏移

    unsigned char status; // led
    dev_t dev_num; // 设备号

    struct cdev led_cdev; // 设备内核文件
};
typedef struct LED_DATA_T led_data_t;

#endif /* __led_drv_h__ */



