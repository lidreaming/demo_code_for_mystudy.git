/** @file         main.c
 *  @brief        简要说明
 *  @details      详细说明
 *  @author       lzm
 *  @date         2020-11-10 17:01:15
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

//ARM 开发板 LED 设备的路径
#define RLED_DEV_PATH "/sys/class/leds/red/brightness"
#define GLED_DEV_PATH "/sys/class/leds/green/brightness"
#define BLED_DEV_PATH "/sys/class/leds/blue/brightness"

int main(int argc, char *argv[])
{
    int res = 0;
    int r_fd;
    int g_fd;
    int b_fd;
    
    printf("this is the led demo\n");

    // 获取 LED 设备文件描述符
    r_fd = open(RLED_DEV_PATH, O_WRONLY);
    if(r_fd < 0){
        printf("Fail to open %s device\n",RLED_DEV_PATH);
        exit(1);
    }
    g_fd = open(GLED_DEV_PATH, O_WRONLY);
    if(g_fd < 0){
        printf("Fail to open %s device\n",GLED_DEV_PATH);
        exit(1);
    }
    b_fd = open(BLED_DEV_PATH, O_WRONLY);
    if(b_fd < 0){
        printf("Fail to open %s device\n",BLED_DEV_PATH);
        exit(1);
    }

    while(1){
        write(r_fd, "255", 3);
        sleep(2);
        write(g_fd, "255", 3);
        sleep(1);
        write(r_fd, "0", 1);
        sleep(2);
        write(g_fd, "0", 1);
        sleep(1);
        write(b_fd, "255", 3);
        sleep(1);
        write(b_fd, "0", 1);
        sleep(1);
    }
}

