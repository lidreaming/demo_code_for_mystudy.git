/** @file         led_dev_a.h
 *  @brief        具体设备。涉及硬件，具体板卡
 *  @details      提供具体led引脚
 *  @author       lzm
 *  @date         2021-03-06 10:18:03
 *  @version      v1.0
 *  @copyright    Copyright By lizhuming, All Rights Reserved
 *
 **********************************************************
 *  @LOG 修改日志:
 **********************************************************
*/


#include "led_resource.h"

